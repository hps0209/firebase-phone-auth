//import { CanActivateFn } from '@angular/router';



import { Injectable, inject } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivateFn,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../service/auth.service';

//@Injectable({
//  providedIn: 'root',
//})
export const authGuard: CanActivateFn = (route, state) => {
    //constructor(public authService: AuthService, public router: Router) { }
    let router = inject(Router);
    let authService = inject(AuthService);

    if (authService.isLoggedIn !== true) {
    window.alert('Access Denied, Only Logged in User Can Access This Page');
    router.navigate(['phone']);
  }
  return true;
};
//export class AuthGuard implements CanActivateFn {
//  constructor(public authService: AuthService, public router: Router) { }

//  canActivate(
//    route: ActivatedRouteSnapshot,
//    state: RouterStateSnapshot
//  ):
//    | Observable<boolean | UrlTree>
//    | Promise<boolean | UrlTree>
//    | boolean
//    | UrlTree {
//    if (this.authService.isLoggedIn !== true) {
//      window.alert('Access Denied, Only Logged in User Can Access This Page');
//      this.router.navigate(['phone']);
//    }
//    return true;
//  }
//}
