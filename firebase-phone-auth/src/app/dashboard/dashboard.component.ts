import { Component, NgZone, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrl: './dashboard.component.css'
})
export class DashboardComponent implements OnInit {
  userData: any;
  constructor(private afAuth: AngularFireAuth,
    private router: Router,
    private ngZone: NgZone) {
  }

  ngOnInit(): void {
    var data = JSON.parse(localStorage.getItem('user_data') || '{}');
    this.userData = data.user.phoneNumber;
    console.log(this.userData);
  }

  logout() {
    return this.afAuth.signOut().then(() => {
      localStorage.removeItem('user_data');
      window.alert('Logged out!');
      this.ngZone.run(() => {
        this.router.navigate(['/phone']);
      });
    });
  }

}
